use std::fs::File;
use std::io::prelude::*;
use std::error::Error;
use std::env;
use std::string::String;

fn case_sensitive_flag_set() -> bool {
    env::vars().filter(|&(ref name, _)| name == "CASE_SENSITIVE")
        .collect::<Vec<(String,String)>>()
        .len() == 1    
}

// use a config data type to execute a program
// by returning a Result type, we can avoid simply panicking,
// which allows us to provide better error messages to end users.
// Box is a 'trait object' and here is being used as a placeholder
// for some type that implements the Error trait. This let's us say
// that we don't know specifically what kind of Error implementation
// it will be, but that it will be one.
pub fn run(config: Config) -> Result<(), Box<Error>> {
    // open the file
    // "?" is like panic! but instead of shutting down, simply
    // returns an Error value for us.
    // It keeps our code cleaner while preserving our type safety
    let mut f = File::open(config.filename)?;

    // create a new string to hold the contents and read them in
    let mut contents = String::new();
    f.read_to_string(&mut contents)?;

    let results = if config.case_sensitive {
        grep(&config.search, &contents)
    } else {
        grep_case_insensitive(&config.search, &contents)
    };

    for line in results {
        println!("{}", line);
    }

    // print them out
    Ok(())
}

// 'a is a lifetime variable, so we are saying that the
// Vec returned by grep will contain string refs that have lifetimes
// equal to the lifetime of the contents variable.
// Without this annotation, the compiler would actually yell at us
// since it knows that it will be returning a slice of a string,
// but doesn't know which string the slice will come from (and thus
// cannot accurately infer the lifetime of the output slice.
fn grep<'a>(search: &str, contents: &'a str) -> Vec<&'a str> {
    let mut results = Vec::new();
    
    for line in contents.lines() {
        if line.contains(search) {
            results.push(line);
        }
    }

    results
}

// for a case-insensitive version of grep, we lowercase each line
// and the search string
fn grep_case_insensitive<'a>(search: &str, contents: &'a str) -> Vec<&'a str> {
    let search = search.to_lowercase();
    let mut results = Vec::new();

    for line in contents.lines() {
        if line.to_lowercase().contains(&search) {
            results.push(line);
        }
    }
    
    results
}

mod test {
    use grep;

    #[test]
    fn one_result() {
        let search = "duct";
        let contents = "\
Rust:
safe, fast, productive.
Pick three.";
        
        assert_eq!(vec!["safe, fast, productive."], grep(search, contents));
    }

    use grep_case_insensitive;
    
    #[test]
    fn case_insensitive() {
        let search = "rust";
        let contents = "\
Rust:
safe, fast, productive.
Pick three.
Trust me.";

        assert_eq!(
            vec!["Rust:", "Trust me."],
            grep_case_insensitive(search, contents)
        );
    }
}


// struct to hold together all of the configuration data
// that our program needs
pub struct Config {
    pub search: String,
    pub filename: String,
    pub case_sensitive: bool,
}

impl Config {
    // constructor for Config
    // borrows a slice of string values
    // "&'static str" is a string lit
    pub fn new(args: &[String]) -> Result<Config, &'static str> {
        // make sure we have an appropriate number of arguments
        if args.len() < 3 {
            return Err("not enough arguments");
        }

        // the config value cannot take ownership of the string values since
        // the func only has access to a slice; so we need to copy.
        let search = args[1].clone();
        let filename = args[2].clone();
        
        Ok(Config {
            search: search,
            filename: filename,
            case_sensitive: case_sensitive_flag_set(),
        })
    }
}
