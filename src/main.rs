extern crate rgrep; // bring code in from src/lib.rs

use std::env;
use std::process;
use std::io::prelude::*;

use rgrep::{Config,run};

/* Common pattern in rust community is to have
 * a main.rs and a lib.rs.
 * lib.rs contains all of the code for program logic
 * while main.rs contains all of the code that handles argument parsing
 * and configuration.
 * i.e. main.rs is for running the program and lib.rs is for defining the program */
fn main() {
    // get handle to stderr for writing error messages
    let mut stderr = std::io::stderr();
    let args: Vec<String> = env::args().collect();

    // try to create a new Config struct and exit if it returns an error
    let config = Config::new(&args).unwrap_or_else(|err| {
        writeln!(
            &mut stderr,
            "could not parse arguments: {}",
            err
        ).expect("coud not write to stderr");
        process::exit(1);
    });

    // since run now returns a Result, the compiler
    // will warn us that we may be overlooking an error if we dont'
    // do something with the Result.
    if let Err(e) = run(config) {
        writeln!(
            &mut stderr,
            "Application error: {}",
            e
        ).expect("Could not write to stderr");
        process::exit(1);
    }
}
